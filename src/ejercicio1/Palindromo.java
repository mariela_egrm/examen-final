/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Mariela Esther Gomez Rivera C.I. 6680592
 */
public class Palindromo {
    
    public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner nf = new Scanner(System.in);
		System.out.println("inserte"
                        + " un palabra: ");
		String palabra = nf.next();
		Stack<Character> pila = new Stack<Character>();
		for (int i = 0; i < palabra.length(); i++) {
			pila.push(palabra.charAt(i));
		}
		String comparar="";
		while(!pila.isEmpty()) {
			comparar=comparar+pila.pop();
		}
		if(palabra.equals(comparar))
			System.out.println("Es un palindromo");
		else
			System.out.println("No es un palindromo");
	}
    
}
