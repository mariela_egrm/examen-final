/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author Mariela Esther Gomez Rivera C.I. 6680592
 */
public class Enteros {
    
  public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner nf = new Scanner(System.in);
		System.out.println("Inserte la cantidad de enteros que pondra: ");
		int n= nf.nextInt();
		Queue<Integer> l = new LinkedList<Integer>();
		for (int i = 0; i < n; i++) {
			l.add(nf.nextInt());
		}
		int res = 0;
		while(!l.isEmpty()) {
			res= res +l.remove();
		}
		System.out.println("el resultado es: "+res);
	}

}
